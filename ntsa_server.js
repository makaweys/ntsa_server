require('dotenv').config();
const http = require('http');
const https = require('https');
const express = require('express');
const axios = require('axios');
const chalk = require('chalk');
const header = require('./lib/header');
const LOG  = console.log;

const fs = require("fs");
const os = require("os");
const path = require("path");

const envFilePath = path.resolve(__dirname, ".env");

const db_connect = require('./lib/db_connect');
const util = require('util');
const { raw } = require('body-parser');
const query = util.promisify(db_connect.query).bind(db_connect);

var danger = chalk.italic.red;
var info = chalk.italic.blue;
var warning = chalk.italic.warning;
var success = chalk.italic.green;
var defaultText = chalk.italic.grey;

var lastID = null;
//Print header/title
header.printHeader();

const app = express();

app.use(express.json());

var db_IMEIs;

const getIMEIs =  async () => {
    var sqlStatement = `SELECT DISTINCT(imei_no) FROM dataoutputinterface_gpsdata WHERE 1`;
    try {
        LOG("Fetching IMEIs... SQL : ", defaultText(sqlStatement));
        //setEnvValue("LAST_ID", 12326512312312);
    
        var results = await query(sqlStatement);
        //var db_IMEIs = (data[0]) ? data[0]: false;
        db_IMEIs = results.map(x => x.imei_no);
        LOG("IMEIs Found: ", db_IMEIs.length);

        return db_IMEIs;
    } catch (error) {
        LOG("Error Fetching IMEIs : ", danger(error));
        return false;
    }

}

app.get('/', function (req, res) {
    console.log("Connection detected");
    data = {'res':true, 'status':"Ping successfull on port 4347"};
    res.status(200).json(data);
});

const getHumanCurrentDate = () => {
    return new Date();
}

const getCurrentDate = () => {
    var serverDate = new Date();
        var yr = serverDate.getFullYear();
        var mnth = serverDate.getMonth()+1;
        var dy = serverDate.getDate();
        var hrs = serverDate.getHours();
        var mins = serverDate.getMinutes();
        var secs = serverDate.getSeconds();

    return yr + "-" + mnth + "-" + dy
}

const createDate = (milliSecs) => {
    var serverDate = new Date(milliSecs);
        var yr = serverDate.getFullYear().toString().substr(-2);
        var mnth = serverDate.getMonth()+1;
        var dy = serverDate.getDate();
        var hrs = serverDate.getHours();
        var mins = serverDate.getMinutes();
        var secs = serverDate.getSeconds();

        var datePiece = mnth + "/" + dy + "/" + yr;
        var timePiece = hrs +":"+mins+":"+secs;

    return { date: datePiece, time:timePiece }
}

const sendData =  async () => {

    try {
        //var fetchedIMEIs = await getIMEIs();
        //LOG("Fetch IMEIS: ", db_IMEIs);

        var server_date = getCurrentDate();
        LOG(defaultText("*******************************************************************************************************"));
        LOG(info("Checking for available data to push to NTSA ..."));
        LOG("serverDate: " + info(getHumanCurrentDate()));
        
        var dateQuery = "FROM_UNIXTIME(data_received_time/1000) > SUBDATE(NOW(), INTERVAL 1 HOUR)";
        /*Remote*/
        // console.log(db_IMEIs);
        var CURRENT_LAST_ID = getEnvValue("LAST_ID")
        if (CURRENT_LAST_ID == "NULL" || CURRENT_LAST_ID == "undefined") {
            LOG(defaultText("Unable to trace the last point..."));
            var sqlStatement = `SELECT * FROM dataoutputinterface_gpsdata WHERE imei_no='202011180206' AND  ${dateQuery} ORDER BY data_received_time DESC LIMIT 1`;
        } else {
            LOG("Last Point : ", success(CURRENT_LAST_ID));
            var sqlStatement = `SELECT * FROM dataoutputinterface_gpsdata WHERE data_received_time >${CURRENT_LAST_ID} LIMIT 200`;
        }

    
        LOG("Searching for data ... SQL : ", defaultText(sqlStatement));
        //setEnvValue("LAST_ID", 12326512312312);
    
        let data = await query(sqlStatement, db_IMEIs);
        var deviceData = (data) ? data: false;
        //LOG("Found Data : ", success(JSON.stringify(deviceData)));
        LOG("Data Length: ", success(data.length));
        

        if (deviceData) {

            if (CURRENT_LAST_ID == "NULL" || CURRENT_LAST_ID == "undefined") {
                //LOG("Found Data : ", success(JSON.stringify(deviceData)));
                var dataInsertTime = deviceData[0].data_insert_time;
                var dataReceivedTime = createDate(deviceData[0].data_received_time);
                
                var postJsonData =  {
                    "date": dataReceivedTime.date,
                    "time": dataReceivedTime.time,
                    "deviceImei": deviceData[0].imei_no,
                    "model": "Model",
                    "carPlate": deviceData[0].vehicle_no,
                    "speed": deviceData[0].speed,
                    "longitude": deviceData[0].longitude,
                    "longitudeDirection": "-",
                    "latitude": deviceData[0].latitude,
                    "latitudeDirection": "-",
                    "powerDisconnection": deviceData[0].di3,
                    "speedSignaldisconnection": deviceData[0].odom
                }

                LOG("Post Data : ", success(JSON.stringify(postJsonData)));
                
                if (postData(postJsonData)) {
                    setEnvValue("LAST_ID", deviceData[0].data_received_time);
                }


            } else {

                deviceData.forEach(async (row, index) => {
                    var dataInsertTime = row.data_insert_time;
                    var dataReceivedTime = createDate(row.data_received_time);
                    //LOG(row);
                    if (index == 0)
                        setEnvValue("LAST_ID", row.data_received_time);


                    var postJsonData =  {
                        "date": dataReceivedTime.date,
                        "time": dataReceivedTime.time,
                        "deviceImei": row.imei_no,
                        "model": "Model",
                        "carPlate": row.vehicle_no,
                        "speed": row.speed,
                        "longitude": row.longitude,
                        "longitudeDirection": "-",
                        "latitude": row.latitude,
                        "latitudeDirection": "-",
                        "powerDisconnection": row.di3,
                        "speedSignaldisconnection": row.odom
                    }

                    LOG("Post Data : ", success(JSON.stringify(postJsonData)));
                    
                    let res = await postData(postJsonData);
                    
                    if (res) {
                        //LOG("Marking LAST ID :", defaultText(row.data_received_time));
                        setEnvValue("LAST_ID", row.data_received_time);
                    }

                });
            }

        }  else {
            LOG(danger("No Data Found"));
        }

    } catch (error) {
        LOG(danger(error));
    }

    LOG(defaultText("*******************************************************************************************************"));
    
};

const postData = async (data) => {
    
    try {
        LOG(defaultText("Posting Data..."))
        
        const httpsAgent = new https.Agent({ rejectUnauthorized: false });
        // var jsonData = qs.stringify(data);
        
        let config = {
                        headers: {
                            "Content-Length": data.length,
                            "Content":data
                        },

                        httpsAgent
                    };
       
        var res = await axios.put('https://www.ntsa.go.ke/speedlimiter/sg_data.php', data, config);
        LOG ("Response" ,res.statusText);
        LOG ("Response Message" ,success(JSON.stringify(res.data)));

        return parseInt(res.data.status);
    } catch (error) {
        LOG("Post Error : ", danger(error));
        return false;
    }
    
};

const readEnvVars = () => fs.readFileSync(envFilePath, "utf-8").split(os.EOL);

const getEnvValue = (key) => {
  // find the line that contains the key (exact match)
  const matchedLine = readEnvVars().find((line) => line.split("=")[0] === key);
  // split the line (delimiter is '=') and return the item at index 2
  return matchedLine !== undefined ? matchedLine.split("=")[1] : null;
};

const setEnvValue = (key, value) => {

    LOG(defaultText("Setting LAST_ID...", value));

    const envVars = readEnvVars();
    const targetLine = envVars.find((line) => line.split("=")[0] === key);
    if (targetLine !== undefined) {
        // update existing line
        const targetLineIndex = envVars.indexOf(targetLine);
        // replace the key/value with the new value
        envVars.splice(targetLineIndex, 1, `${key}="${value}"`);
    } else {
        // create new key value
        envVars.push(`${key}="${value}"`);
    }
    // write everything back to the file system
    fs.writeFileSync(envFilePath, envVars.join(os.EOL));
};





//  setTimeout( function()  { sendData(); },  3000 );
setTimeout( function()  { getIMEIs(); },  3000 );
setTimeout( function()  { sendData(); },  10000 );

setInterval( function()  { sendData(); },  60000 );
setInterval( function()  { getIMEIs(); },  86400000 );


const server = http.createServer(app);
const port = process.env.PORT;
server.listen(port);

console.debug('Server listening on port ' + port);

