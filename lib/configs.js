var configs = {
    "app_name": "NTSA Server 1.0",
    "developer":"Makaweys",
    "for":"Silicon Valley Solutions (SVS)",
    "dev_year":"2020",
    "version":"1.0.0",
    "config": {
        "host": 'localhost',
        "port": 4347,
        "exclusive": true
    },
    
    "inboundData" : {
        stringLength: 27,
        heartbeatLength: 15
    },
    

}

module.exports = configs;