require('dotenv').config();
const chalk = require('chalk');
var configs = require('./configs');
var mysql = require('mysql');



const db_configs = {
    host:process.env.DB_HOST,
    database:process.env.DB_NAME,
    user:process.env.DB_USER,
    password:process.env.DB_PASS
};

// Initiate connection    
db_connect = mysql.createConnection(db_configs);



db_connect.connect((err) => {
    if(err){
        console.log(chalk.italic.bgWhite.black("Error connecting to Db"));
        return;
    }
    
    console.log(chalk.italic.bgWhite.black("Connected to mysql db :" + db_configs.database));

});

db_connect.on('error', function(err) {
    console.log("[mysql error] ", err); 
});

module.exports = db_connect;