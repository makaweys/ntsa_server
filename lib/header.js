const clear = require('clear');
configs = require('./configs');
const figlet = require('figlet');
const chalk = require('chalk');
        
var header = {
    
    printHeader: function() {
        clear();

        console.log(
            chalk.bold.yellow(
            figlet.textSync('NTSA Server 1.O', { horizontalLayout: 'full' })
            )
        );

        console.log(chalk.yellow("========================================================================================"));
        console.log(chalk.bold.bgWhite.black(configs.app_name + " v" + configs.version + " Developed By: " + configs.developer + 
                                    " For: " + configs.for + " - " + configs.dev_year));
        console.log(chalk.yellow("========================================================================================"));                            
    }
};

module.exports = header;